SRCS = $(shell ls *.c)
OBJS = $(SRCS:%.c=%.o)
TARGETS = $(BIN_PATH)/ats-send $(BIN_PATH)/ats-respond $(LIB_PATH)/libats.a

CFLAGS := $(CFLAGS_STD) $(CFLAGS_COMMON) $(CFLAGS_APRIL_LCMTYPES)
LDFLAGS := $(LDFLAGS_STD) $(LDFLAGS_COMMON) $(LDFLAGS_APRIL_LCMTYPES)
DEPS := $(DEPS_STD) $(DEPS_COMMON) $(DEPS_APRIL_LCMTYPES)

include $(BUILD_COMMON)


all: $(TARGETS)
	@true

$(BIN_PATH)/ats-send: ats_send.o $(DEPS)
	@$(LD) -o $@ $^ $(LDFLAGS)

$(BIN_PATH)/ats-respond: ats_respond.o $(DEPS)
	@$(LD) -o $@ $^ $(LDFLAGS)

$(LIB_PATH)/libats.a: ats.o $(DEPS)
	@$(AR) rc $@ $^

clean:
	@rm -rf *.o $(TARGETS)
