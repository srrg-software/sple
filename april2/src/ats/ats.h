/* Copyright (C) 2013-2016, The Regents of The University of Michigan.
All rights reserved.

This software was developed in the APRIL Robotics Lab under the
direction of Edwin Olson, ebolson@umich.edu. This software may be
available under alternative licensing terms; contact the address above.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PINGPONG_HEADER
#define PINGPONG_HEADER

#include <unistd.h>
#include <arpa/inet.h>
#include <inttypes.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>

#include <lcm/lcm.h>

#include "common/time_util.h"
#include "common/net_util.h"


#define PING_PORT 7901
#define ATS_PACKET_SIZE 4+8+8+4
#define ATS_PING_MAGIC 0x23410001
#define ATS_PONG_MAGIC 0x14320001

// "PING" Wire Spec:
//  This message consists of four magic bytes, followed by a 64-bit utime,
//  sent MSB. (usecs since epoch)
// |----|----|----|----|-4--|.....|
// |0x89|0x67|0x45|0x23|port| MSB.....LSB
//
// "PONG" Wire Spec:
//  This message consists of four magic bytes, followed by a 64-bit utime
//  difference (recv_time - ping_time), followed by this system's utime, given as
//  in utime_now()
//
// |----|----|----|----|-----8----|----8----|
// |0x98|0x76|0x54|0x32|MSB...LSB|MSB...LSB|

// LCM SPEC:
// lcmint64_t:
// utime: time this measurement was taken ON THIS MACHINE
// data:  use as: raw_utime + data = host_corrected_utime
//
// data (offset) is computed as:
//  offset = (diff2 - diff1)/2
//
// No smoothing is applied
//
// --------- UTIMES -------------
// local_event_time = ats_offset + remote_event_time

typedef struct ats_tracker ats_tracker_t;


// regex should normally be "ATS_.*", smoothing is an EWMA alpha,
// where 0 is no smoothing, 1 would just grab the first measurement
ats_tracker_t * ats_tracker_create(lcm_t * lcm, const char * regex, double smoothing);
int64_t ats_tracker_get(ats_tracker_t * ats, int64_t id);

#endif //PINGPONG_HEADER
