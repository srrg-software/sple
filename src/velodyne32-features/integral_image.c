/* Copyright (C) 2013-2016, The Regents of The University of Michigan.
All rights reserved.

This software was developed in the APRIL Robotics Lab under the
direction of Edwin Olson, ebolson@umich.edu. This software may be
available under alternative licensing terms; contact the address above.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the Regents of The University of Michigan.
*/

#include <stdlib.h>
#include <stdio.h>

#include <omp.h>

#include "integral_image.h"
#include "point_cloud.h"

integral_image_t* integral_image_create(int width, int height) {
  integral_image_t *im = (integral_image_t*)calloc(1, sizeof(integral_image_t));

  im->width = width;
  im->height = height;

  im->buf = calloc(im->height * im->width, sizeof(point_accumulator_t));

  point_accumulator_t *buf = im->buf;
  for(int r = 0; r < im->height; ++r) {
    for(int c = 0; c < im->width; ++c, ++buf) {
      buf->sum = matd_create(4, 1);
      buf->squared_sum = matd_create(4, 4);
    }
  }

  return im;
}

void integral_image_destroy(integral_image_t *im) {
  point_accumulator_t *buf = im->buf;
  for(int r = 0; r < im->height; ++r) {
    for(int c = 0; c < im->width; ++c, ++buf) {
      matd_destroy(buf->sum);
      matd_destroy(buf->squared_sum);
    }
  }
  
  free(im->buf);
  free(im);
}

void integral_image_clear(integral_image_t *im) {
#pragma omp parallel for
  for(int r = 0; r < im->height; ++r) {
    for(int c = 0; c < im->width; ++c) {
      point_accumulator_clear(im->buf + r * im->width + c);
    }
  }
}

void integral_image_point_compute(integral_image_t *integral_image, 
				  mati_t *indices, point_cloud_t *point_cloud) {
  assert(integral_image->width > 0 && integral_image->height > 0);    
  assert(indices->ncols == integral_image->width && indices->nrows == integral_image->height);
  assert(zarray_size(point_cloud->measurements) > 0);
    
  integral_image_clear(integral_image);

  // Fill the accumulators with the points
#pragma omp parallel for
  for(int r = 0; r < integral_image->height; ++r) {
    int* index = indices->data + r * indices->ncols;
    for(int c = 0; c < integral_image->width; ++c, ++index) {
      point_accumulator_t *pa = integral_image->buf + r * integral_image->width + c;
      if(*index < 0) {
	continue;
      }
      rich_point_t *rp;
      zarray_get_volatile(point_cloud->measurements, *index, &rp); 
      point_accumulator_add_point_inplace(pa, rp->point);
    }
  }
  
  // Fill by column
#pragma omp parallel for
  for(int c = 0; c < integral_image->width; ++c) {
    for(int r = 1; r < integral_image->height; ++r) {
      point_accumulator_t *curr_pa = integral_image->buf + r * integral_image->width + c;
      point_accumulator_t *prev_pa = integral_image->buf + (r - 1) * integral_image->width + c;
      point_accumulator_add_inplace(curr_pa, prev_pa);
    }
  }
  
  // Fill by row
#pragma omp parallel for
  for(int r = 0; r < integral_image->height; ++r) {
    for(int c = 1; c < integral_image->width; ++c) {
      point_accumulator_t *curr_pa = integral_image->buf + r * integral_image->width + c;
      point_accumulator_t *prev_pa = integral_image->buf + r * integral_image->width + c - 1;
      point_accumulator_add_inplace(curr_pa, prev_pa);      
    }
  }
}

void integral_image_normal_compute(integral_image_t *integral_image, 
				   mati_t *indices, point_cloud_t *point_cloud) {
  assert(integral_image->width > 0 && integral_image->height > 0);    
  assert(indices->ncols == integral_image->width && indices->nrows == integral_image->height);
  assert(zarray_size(point_cloud->measurements) > 0);
    
  integral_image_clear(integral_image);

  // Fill the accumulators with the points
#pragma omp parallel for
  for(int r = 0; r < integral_image->height; ++r) {
    int *index = indices->data + r * indices->ncols;
    for(int c = 0; c < integral_image->width; ++c, ++index) {
      point_accumulator_t *pa = integral_image->buf + r * integral_image->width + c;
      if(*index < 0) {
	continue;
      }
      rich_point_t *rp;
      zarray_get_volatile(point_cloud->measurements, *index, &rp); 
      matd_put(rp->normal, rp->normal->nrows - 1, 0, 1.0);
      point_accumulator_add_point_inplace(pa, rp->normal);
      matd_put(rp->normal, rp->normal->nrows - 1, 0, 0.0);
    }
  }
  
  // Fill by column
#pragma omp parallel for
  for(int c = 0; c < integral_image->width; ++c) {
    for(int r = 1; r < integral_image->height; ++r) {
      point_accumulator_t *curr_pa = integral_image->buf + r * integral_image->width + c;
      point_accumulator_t *prev_pa = integral_image->buf + (r - 1) * integral_image->width + c;
      point_accumulator_add_inplace(curr_pa, prev_pa);
    }
  }
  
  // Fill by row
#pragma omp parallel for
  for(int r = 0; r < integral_image->height; ++r) {
    for(int c = 1; c < integral_image->width; ++c) {
      point_accumulator_t *curr_pa = integral_image->buf + r * integral_image->width + c;
      point_accumulator_t *prev_pa = integral_image->buf + r * integral_image->width + c - 1;
      point_accumulator_add_inplace(curr_pa, prev_pa);      
    }
  }
}

int _clamp(int v, int min, int max) {
  v = (v < min) ? min : v;
  v = (v > max) ? max : v;
  return v;
}

point_accumulator_t* integral_image_get_region(integral_image_t *integral_image,
					       int row_min, int row_max, 
					       int col_min, int col_max) {
  assert(integral_image->width > 0 && integral_image->height > 0);    
  point_accumulator_t *pa = point_accumulator_create();
  row_min = _clamp(row_min - 1, 0, integral_image->height - 1);
  row_max = _clamp(row_max, 0, integral_image->height - 1);
  col_min = _clamp(col_min - 1, 0, integral_image->width - 1);
  col_max = _clamp(col_max, 0, integral_image->width - 1);
  point_accumulator_add_inplace(pa, 
				integral_image->buf + 
				row_max * integral_image->width + col_max); // Whole region
  point_accumulator_add_inplace(pa,
				integral_image->buf +
  				row_min * integral_image->width + col_min); // Upper left region
  point_accumulator_sub_inplace(pa,
  				integral_image->buf +
  				row_min * integral_image->width + col_max); // Upper region
  point_accumulator_sub_inplace(pa,
  				integral_image->buf +
  				row_max * integral_image->width + col_min); // Leftmost region
  return pa;  
}
